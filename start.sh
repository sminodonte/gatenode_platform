#!/bin/bash
 
# STOP AND DELETE gate-node_platform CONTAINER 
docker-compose down

# START A NEW CONTAINER FOR THE gate-node_platform
docker-compose up -d
