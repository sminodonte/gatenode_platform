# GATE-NODE PLATFORM

## Run 
Run Gate-Node to make your device a node on the network Middle-Node.

Follow the steps below:

#### First time
1. Clone repository:

    ```
    $ git clone https://github.com/sminodonte/gatenode_platform.git
    ```

2. Get inside directory:

    ```
    $ cd gatenode_platform
    ```

3. Edit the daemon.json file and follow these steps:
    - On linux
        - go to /etc/docker and edit the file deamon.json:
        
            ```
            $ { "insecure-registries" : ["156.148.132.82:5000","192.168.193.139:5000"] }
            ```

        - Reboot the machine from shell:
        
            ```
            $ sudo reboot
            ```

    - For other platforms:
        - follow these steps https://docs.docker.com/registry/insecure

4. Set variables environment:
    - Before starting the platform you have to init the environment variables both for single container execution and for orchestration purposes. This repo provides you two default environment files called:
        - env-file.default containing the variables for docker images configuration
        - .env.default containing the variables for docker compose configuration

    - You have just to rename and set the proper variables' values:
        - copy and rename the environment files:

            ```
            $ cp env-file.default env-file
            $ cp .env.default .env
            ```

        - Only if strictly necessary, set the variables properly:
        
            ```
            $ nano env-file 
            $ nano .env
            ```


5. Service setup:
    - To run, __ONLY FIRST TIME__, the composed/orchestrated services follow the command:
    
        ```
        $ /bin/bash init.sh
        ```

        Now the __GATE-NODE__ is runnning.
    - To shutdown the system and remove all service containers run the following command:
    
        ```
        $ /bin/bash stop.sh
        ```

    - To restart the services run the following command:
    
        ```
        $ /bin/bash start.sh
        ```


#### For every other time to start and stop the existing Gate-Node service use ONLY these two commands:
- To run the composed/orchestrated services:

    ```
    $ /bin/bash start.sh
    ```

- To shutdown the system and remove all service containers:

    ```
    $ /bin/bash stop.sh
    ```

    
> __IMPORTANT!__ 
Every time the init.sh is launched the system is reset and a new node will be created, for this reason it is important not to use it more than once. Then always use start.sh to restart the system. 

#### Uninstall gate-node
1. On shell execute:

    ```
    $ /bin/bash uninstall.sh
    ```

    
## Connect
Once Gate-Node is activated, another application can connect to it and may send it a message via socket.io with this type of configuration:

### Services


 | PROTOCOL | HOST | PORT |
 | ------ | ------ | ------ | 
 | socketIO | 127.0.0.1 | 3002 |


### Channels


| CHANNEL | DATA TYPE |
| ------ | ------ | 
| gatenode | little messages | 
| gatenode_p2p | big data | 


### Data format for each channel
- __gatenode__:

    ```
    {
      "sender": "node0",
      "receiver": {
        "name": ["node1","node2","node3","node4"],
        "type": "compute", // [stream, compute, collect, view]
        "query": "name" // [name, type]
      },
      "data": {
        "payload": {"online":"message"}
      }
    }
    ```

- __gatenode_p2p__:

     ```
    {
      "sender": "node0",
      "receiver": {
        "name": "API_LAYER" 
      },
      "data": {
        "protocol": "udp", // [udp, zeromq, osc, ...]
        "role": "sender",  // [sender, receiver, *]
        "payload": {"algorithm":"tracked_objects", "payload":"" } // This Object is the algorithm's output
      }
    }
     ```
     
