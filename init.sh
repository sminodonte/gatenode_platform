#!/bin/bash
source .env

# STOP AND DELETE gate-node_platform CONTAINER 
docker-compose down
sleep 2s

# REPLACE VALUE FROM TRUE TO FALSE IN GATE_NODE_INIT
#perl -pi -e 's/true/false/g' env-file 

# DELETE NODE FROM NETWORK
eval "$(grep ^GATE_NODE_DEVICE_UUID= do_not_touch_file)"
eval "$(grep ^GATE_NODE_DEVICE_TOKEN= do_not_touch_file)"
eval "$(grep ^GATE_NODE_DEVICE_INIT_UUID= do_not_touch_file)"
eval "$(grep ^GATE_NODE_DEVICE_INIT_TOKEN= do_not_touch_file)"
eval "$(grep ^MIDDLENODE_HOSTNAME= do_not_touch_file)"
eval "$(grep ^MIDDLENODE_HTTP_PORT= do_not_touch_file)"


curl -X DELETE \
  http://"${MIDDLENODE_HOSTNAME}":"${MIDDLENODE_HTTP_PORT}"/devices/$GATE_NODE_DEVICE_UUID \
  -H "meshblu_auth_token: ${GATE_NODE_DEVICE_TOKEN//[$'\t\r\n ']}" \
  -H "meshblu_auth_uuid: ${GATE_NODE_DEVICE_UUID//[$'\t\r\n ']}
"
curl -X DELETE \
  http://"${MIDDLENODE_HOSTNAME}":"${MIDDLENODE_HTTP_PORT}"/devices/$GATE_NODE_DEVICE_INIT_UUID \
  -H "meshblu_auth_token: ${GATE_NODE_DEVICE_INIT_TOKEN//[$'\t\r\n ']}" \
  -H "meshblu_auth_uuid: ${GATE_NODE_DEVICE_INIT_UUID//[$'\t\r\n ']}
"

# load configuration variables
sudo rm -rf do_not_touch_file

if [ "$(uname)" == "Darwin" ]; then
    # SHOW ALL IP ADDRESS OF ALL INTERFACE
    public_ip=$(curl ipecho.net/plain ; echo)
    local_ip=$public_ip #$(ipconfig getifaddr en0)
    local_vpn_ip=$(ifconfig  | awk  '$1 = "inet" {print $2}' | grep 10.147.18)   
    export PUBLIC_IP=$public_ip LOCAL_IP=$local_ip LOCAL_VPN_IP=$local_vpn_ip
    ALLIP='$PUBLIC_IP:$LOCAL_IP:$LOCAL_VPN_IP'
    envsubst "$ALLIP" < "env-file" > "do_not_touch_file"
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    # SHOW ALL IP ADDRESS OF ALL INTERFACE
    public_ip=$(curl ipinfo.io/ip)
    local_ip=$public_ip #$(ifconfig eno1 | grep -i mask | awk '{print $2}'| cut -f2 -d:)
    local_vpn_ip=$(echo `hostname -I` | tr ' ' '\n' | grep 10.147.18) #$(ifconfig  | awk  '$1 = "inet" {print $2}' | grep 10.147.18)   
    # public_ip=$(curl ipecho.net/plain ; echo) wlan0
    # local_ip=$(ifconfig wlan0 | grep -i mask | awk '{print $2}'| cut -f2 -d:)
    # local_vpn_ip=$(ip -o -4 addr show | awk '{print $2 " " $4}' | awk '/^ztqu/ { print }' | awk '{print $2}' | awk -F'[/ ]+' '{ print $1 }')
    export PUBLIC_IP=$public_ip LOCAL_IP=$local_ip LOCAL_VPN_IP=$local_vpn_ip
    ALLIP='$PUBLIC_IP:$LOCAL_IP:$LOCAL_VPN_IP'
    envsubst "$ALLIP" < "env-file" > "do_not_touch_file"
fi

# DELETE DATABASE AND LOGS
sudo rm -rf data
sudo rm -rf logs

# Install Zerotier and add to network
curl -s https://install.zerotier.com/ | sudo bash
sudo zerotier-cli join $VPN_NETWORK
sudo zerotier-cli status
sleep 2s

# DELETE ALL IMAGES OF gate-node platform
docker rmi $GATENODE_CORE_IMAGE
docker rmi $GATENODE_GUI_IMAGE

# START A NEW CONTAINER FOR THE gate-node_platform
docker-compose up -d